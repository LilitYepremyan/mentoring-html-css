# Mentoring-HTML-CSS

## Name:  Lilit Yepremyan


__Country__: Yerevan, Armenia

__Email__: lilith.yepremyan@gmail.com
 ### Achievements:
I have made many achievements in my life.  I am happy to be such an achiever at things. I remember my first achievement which was in kindergarten. In kindergarten I got on the honor roll for the first time.
My achievements are the best moments in my life. Each of my achievement has signified that I have accomplished something.

My achievements have made me the person I am today and they form an integral part of my life even if I merely have memories of them.

In my working period there was an issue with accounts payable . A lot of money was missing, and none of my supervisors could figure out what happened. Most of the team thought it was stolen. I took the time outside my designated duties to trace the money route for the day and eventually found that the money was in a queue for the wrong vendor. We were able to remedy the issue quickly, and my supervisors were very thankful that I took the time to help out.

### Other:
* I am self-starter
* Team player
* Quick learner
* Self-driven
* Sociable person
